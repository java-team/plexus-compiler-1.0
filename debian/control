Source: plexus-compiler-1.0
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Torsten Werner <twerner@debian.org>,
           Ludovic Claude <ludovic.claude@laposte.net>,
           Miguel Landaeta <nomadium@debian.org>
Build-Depends: debhelper (>= 9), default-jdk, maven-debian-helper (>= 1.5)
Build-Depends-Indep: default-jdk-doc,
                     junit (>= 3.8.2),
                     libcommons-lang-java (>= 2.0),
                     libecj-java,
                     libmaven-javadoc-plugin-java,
                     libmaven2-core-java,
                     libmaven2-core-java-doc,
                     libplexus-component-metadata-java,
                     libplexus-container-default-java,
                     libplexus-container-default-java-doc,
                     libplexus-utils-java (>= 1.5.15),
                     libplexus-utils-java-doc
Standards-Version: 3.9.6
Vcs-Git: git://anonscm.debian.org/pkg-java/plexus-compiler-1.0.git
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=pkg-java/plexus-compiler-1.0.git
Homepage: http://plexus.codehaus.org/plexus-components/plexus-compiler/

Package: libplexus-compiler-1.0-java
Architecture: all
Depends: libcommons-lang-java (>= 2.6),
         libmaven2-core-java,
         libplexus-container-default-java,
         libplexus-utils-java,
         ${misc:Depends}
Suggests: junit (>= 3.8.2), libecj-java, libplexus-compiler-1.0-java-doc
Provides: libplexus-compiler-java
Conflicts: libplexus-compiler-java (<< 1.9.2-3)
Replaces: libplexus-compiler-java (<< 1.9.2-3)
Description: Plexus compiler system
 The Plexus project provides a full software stack for creating and executing
 software projects. Based on the Plexus container, the applications can
 utilise component-oriented programming to build modular, reusable components
 that can easily be assembled and reused.
 .
 This package provides the Plexus Compiler API and its implementation modules
 supporting javac, jikes, eclipse, aspectj and csharp compilers.

Package: libplexus-compiler-1.0-java-doc
Architecture: all
Section: doc
Depends: ${maven:DocDepends}, ${misc:Depends}
Recommends: ${maven:DocOptionalDepends}
Suggests: libplexus-compiler-1.0-java
Description: Documentation for The API for the Plexus compiler system
 The Plexus project provides a full software stack for creating and executing
 software projects. Based on the Plexus container, the applications can
 utilise component-oriented programming to build modular, reusable components
 that can easily be assembled and reused.
 .
 libplexus-compiler-1.0-java provides the Plexus Compiler API and its
 implementation modules supporting javac, jikes, eclipse, aspectj and csharp
 compilers.
 .
 This package contains the API documentation of libplexus-compiler-1.0-java.
